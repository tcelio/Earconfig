package br.com.indra.earconfig.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.indra.earconfig.bean.CfgBean;

public class CfgAuditor {

	String key_pontos;
	String value_pontos;
	String key_igualdade;
	String value_igualdade;
	String line;
	// public final static int case;
	int posicao_duplo_ponto;
	int posicao_do_igual;
	int count_igualdade;
	int count_dois_pontos;
	int value_separation_pipe;
	int value_separation_virgula;
	int value_separation_ecomercial;
	int value_separation_ponto_virgula;

	int count_ponto_virgula;
	int count_pipeline;
	int count_ecomercial;
	int count_virgula;

	int posicao_interrogacao;
	int count_interrogacao;

	int value_separation_igualdade;
	int reference_numero;
	
	ArrayList<CfgBean> listaPrincipal = new ArrayList<CfgBean>();

	public CfgAuditor() {
	    //this.cfgBean = null;
		this.listaPrincipal.clear();
	}

	public void verificarCaso(String line) {

		try{
			String reference_ = "";
			this.line = line.trim();// retira espa�os
			this.posicao_do_igual = line.indexOf("=");
			this.posicao_duplo_ponto = line.indexOf(":");
			this.count_igualdade = StringUtils.countMatches(line, "=");
			this.count_dois_pontos = StringUtils.countMatches(line, ":");
		
	     		int keyType = verificarKeyType(posicao_do_igual, posicao_duplo_ponto); // para verificar o tipo de key ":"(1 - dois pontos) ou (2 - igual(=))

				if (keyType != -1 && keyType == 1) {// 1 => dois pontos (":")

					this.key_pontos = line.substring(0, posicao_duplo_ponto);
					this.value_pontos = line.substring(posicao_duplo_ponto + 1, line.length());
					
						//this.count_interrogacao = StringUtils.countMatches(value_pontos, "?");
						this.count_ponto_virgula = StringUtils.countMatches(value_pontos, ";");
						this.count_pipeline = StringUtils.countMatches(value_pontos, "|");
						this.count_ecomercial = StringUtils.countMatches(value_pontos, "&");
						this.count_virgula = StringUtils.countMatches(value_pontos, ",");
						reference_ = verificarTipoDivisao();
						this.reference_numero = countTipoDivisao();
		
						inserirObjeto(key_pontos, ":", value_pontos, reference_);
				}
				if (keyType != -1 && keyType == 2) {// 2 => igual("=")

					this.key_igualdade = line.substring(0, posicao_do_igual);
					this.value_igualdade = line.substring(posicao_do_igual + 1, line.length());
					
						//this.count_interrogacao = StringUtils.countMatches(value_igualdade, "?");
						this.count_ponto_virgula = StringUtils.countMatches(value_igualdade, ";");
						this.count_pipeline = StringUtils.countMatches(value_igualdade, "|");
						this.count_ecomercial = StringUtils.countMatches(value_igualdade, "&");
						this.count_virgula = StringUtils.countMatches(value_igualdade, ",");
						reference_ = verificarTipoDivisao();
						this.reference_numero = countTipoDivisao();
		
						inserirObjeto(key_igualdade, "=", value_igualdade, reference_);
					
				}
				if (keyType != -1 && keyType == 0) {// 2 => igual("=")

					//this.key_igualdade = line.substring(0, posicao_do_igual);
					//this.value_igualdade = line.substring(posicao_do_igual + 1, line.length());
					
						//this.count_interrogacao = StringUtils.countMatches(value_igualdade, "?");
						
		
						inserirObjeto(line, "", "", "");
					
				}
		}catch(Exception ex){
			System.out.println("Error:"+ex.getMessage()+"-"+ex.getCause());
		}
		
	}
	
	
	public String verificarTipoDivisao(){
		String reference = "";
				// comprovamos que "count_virgula" nao � o maior
		if (count_ponto_virgula >= count_pipeline && count_ponto_virgula >= count_ecomercial
				&& count_ponto_virgula >= count_virgula) {
			reference = ";";
			// comprovado que o "count_ponto_virgula" nao � o maior
		} else if (count_pipeline >= count_ecomercial && count_pipeline >= count_virgula) {
			if(count_pipeline == count_virgula){
				reference = ",";
			}else{
				reference = "|";
			}
			// comprovado que o "count_pipeline" � o maior
		} else if (count_ecomercial >= count_virgula) {
			reference = "&";
		}else{ 
			reference = ",";
		}
		
		if(reference.matches("")){
			if(count_ponto_virgula == count_pipeline || count_ponto_virgula == count_ecomercial ||
				count_ponto_virgula == count_virgula){
					if(count_ponto_virgula == count_pipeline){
						reference = ";";
					}
					if(count_ponto_virgula == count_ecomercial){
						reference = ";";
					}
					if(count_ponto_virgula == count_virgula){
						reference = ";";
					}
			}
				
			if(count_pipeline == count_ecomercial || count_pipeline == count_virgula || 
						count_ecomercial == count_virgula){
				
					if(count_pipeline == count_ecomercial){
						reference = "|";
					}if(count_pipeline == count_virgula){
						reference = ",";
					}if(count_ecomercial == count_virgula){
						reference = ",";
					}
			}
		}
		return reference;
	}
	
	public int countTipoDivisao(){
		int reference_num = -1;
		if (count_interrogacao > count_ponto_virgula && count_interrogacao > count_pipeline
				&& count_interrogacao > count_ecomercial && count_interrogacao > count_virgula) {
			reference_num =count_interrogacao;
			// comprovamos que "count_virgula" nao � o maior
		} else if (count_ponto_virgula > count_pipeline && count_ponto_virgula > count_ecomercial
				&& count_ponto_virgula > count_virgula) {
			reference_num = count_ponto_virgula;
			// comprovado que o "count_ponto_virgula" nao � o maior
		} else if (count_pipeline > count_ecomercial && count_pipeline > count_virgula) {
			reference_num = count_pipeline;
			// comprovado que o "count_pipeline" � o maior
		} else if (count_ecomercial > count_virgula) {
			reference_num = count_ecomercial;
		}else{
			reference_num = count_virgula;
		}
		return reference_num;
	}



	public ArrayList<CfgBean> retornarListaObjetos() {
		return this.listaPrincipal;
	}

	/**
	 * Verifica se o key � do tipo : key : value ==> 1 ou key = value ==> 2
	 * 
	 * @param igualdade_count
	 * @param two_points_count
	 * @return
	 */
	public int verificarKeyType(int igualdade_count, int two_points_count) {
		int keytype = -1;
		if (two_points_count > 0 & igualdade_count > 0) {// verificar se ha ":"
															// "=" ao mesmo
															// tempo na linha
				if (two_points_count > igualdade_count) {
					keytype = 2;
				} else {
					keytype = 1; // 1 - tipo ":" no key (primeiro elemento)
				}
				
		} else if((two_points_count > 0 && igualdade_count == -1) || two_points_count == -1 && igualdade_count > 0){ // caso mais comum. um dos elementos sendo zero e outro
					// existindo
			if (two_points_count > igualdade_count) {
				keytype = 1; // 1 - tipo ":" no key (primeiro elemento)
			} else {
				keytype = 2;
			}
		}else if((two_points_count == -1 && igualdade_count == -1) || (two_points_count == 0 && igualdade_count == 0)){
			keytype = 0;
		}

		return keytype;
	}

	public void inserirObjeto(String key, String igualdade, String value, String division) {
		CfgBean cfgBean = new CfgBean();
		List<String> lista = new ArrayList<String>();
		
//		if(key.matches("") || key.matches(null)){
//			key = "VAZIO";
//		}
		try {
			lista = obterListaComElementoEspecifico(value, division);
			cfgBean.setIgualdade(igualdade);
			cfgBean.setKey(key);
			cfgBean.setList(lista);
			this.listaPrincipal.add(cfgBean);
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+"-"+e.getCause());
			e.getStackTrace();
		}

	}

	
	public List<String> obterListaComElementoEspecifico(String lineValue,String splitter) {

		List<String> lista_ = new ArrayList<String>();
		
		if(!splitter.isEmpty()){
			
			
			String[] parts = lineValue.split("\\"+splitter);
			try {
				while (reference_numero >= 0) {
					lista_.add(parts[reference_numero]);
					reference_numero--;
				}
			} catch (Exception e) {
				System.out.println("Erro: " + e.getCause());
			}
		}else{
			lista_.add("");
		}
		
		return lista_;
	}
	
	
}
