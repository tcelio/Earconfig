package br.com.indra.earconfig.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.html.HTML.Tag;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.indra.earconfig.bean.CfgBean;
import br.com.indra.earconfig.bean.XmlContent;

public final class FileUtils {

	public static final String SEPARATOR = "/";
	static int status = 0;
	public static ArrayList<XmlContent> xmlContentListOne = new ArrayList<XmlContent>();
	public static ArrayList<XmlContent> xmlContentListTwo = new ArrayList<XmlContent>();
	
	public static ArrayList<XmlContent> xmlVerificadorList = new ArrayList<XmlContent>();
	public static ArrayList<XmlContent> xmlVerificadorListInversa = new ArrayList<XmlContent>();

	public static ArrayList<CfgBean> cfgContentListOne = new ArrayList<CfgBean>();
	public static ArrayList<CfgBean> cfgContentListTwo = new ArrayList<CfgBean>();
	public static int last_repeat_reference_list;
	public static int last_repeat_target_list;
	public static String tag_xml_value = "";
	
	public static String xml_referencia;
	
	private FileUtils() {
		xmlContentListOne = null;
		xmlContentListTwo = null;
		xmlContentListOne.clear();
		xmlContentListTwo.clear();
		xmlContentListOne.trimToSize();
		xmlContentListTwo.trimToSize();
		status = 0;
		xml_referencia = "";
	}

	public static void copy(String src, String dest) throws IOException {
		Files.copy(new File(src).toPath(), new File(dest).toPath(),
				StandardCopyOption.REPLACE_EXISTING);
	}

	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 * getPathName
	 */
	public static String extractFileNameAndPath(String fullFileName) {
		int indexOf = fullFileName.lastIndexOf(SEPARATOR);
		if (indexOf > 0) {
		  return fullFileName.substring(0,indexOf);
		  
		} else {
			return fullFileName;
		}
	}
	
	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 * getPathName
	 */
	public static String takeOffFirstDirectoryPath(String fullFileName) {
		int indexOf = fullFileName.indexOf(SEPARATOR);
		if (indexOf > 0) {
		  return fullFileName.substring(indexOf,fullFileName.length());
		  
		} else {
			return fullFileName;
		}
	}
	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 * getPathName
	 */
	public static String takeOffFirstDirectoryPathPlus(String fullFileName) {
		int indexOf = fullFileName.indexOf(SEPARATOR);
		if (indexOf > 0) {
		  return fullFileName.substring(indexOf+1,fullFileName.length());
		  
		} else {
			return fullFileName;
		}
	}
	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 */
	public static String extractFileName(String fullFileName) {
		int indexOf = fullFileName.lastIndexOf(SEPARATOR);
		if (indexOf > 0) {
			return fullFileName.substring(indexOf + 1);
		} else {
			return fullFileName;
		}
	}
	/**
	 * Extrair pasta do arquivo informado baseado em {@link FileUtils.SEPARATOR}
	 * .
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo.
	 * @return Pasta
	 */
	public static String extractFilePath(String fullFileName) {
		int indexOf = fullFileName.lastIndexOf(SEPARATOR);
		if (indexOf > 0) {
			return fullFileName.substring(0, indexOf);
		} else {
			return "";
		}
	}

	/**
	 * Extrair pasta do arquivo informado baseado em {@link FileUtils.SEPARATOR}
	 * .
	 * 
	 * @param fullFileFolder
	 *            - Caminho completo da pasta.
	 * 
	 * @return Nome da ultima Pasta
	 */
	public static String extractLastFolder(String fullFileFolder) {
		return new File(fullFileFolder).getName();
	}

	public static boolean createDirs(String pathToCreate) {
		return new File(pathToCreate).mkdirs();
	}

	public static boolean exists(String pathToCheck) {
		return new File(pathToCheck).exists();
	}

	public static File[] listFilesFromFolder(String folder) {
		File file = new File(folder);
		return file.listFiles();
	}

	/**
	 * M�todo para gera��o de arquivo Properties, respons�vel pelo mapeamento
	 * dos arquivos para os respectivos jars.
	 * 
	 * Al�m disso, possui o par�metro "output" que especifica o nome do arquivo
	 * a ser gerado nos comandos -d e -f.
	 * 
	 * @param configDirectory
	 *            - Diretorio onde o arquivo properties ser� gerado.
	 * @param configFileName
	 *            - Nome de arquivo a ser gerado
	 * @param propertyMap
	 *            - Map de propriedades a serem salvas.
	 */
	public static void savePropertyFile(String configDirectory,
			String configFileName, Map<?, ?> propertyMap) {
		final Properties prop = new Properties() {
			private static final long serialVersionUID = 1L;

			@Override
			public synchronized Enumeration<Object> keys() {
				return Collections.enumeration(new TreeSet<Object>(super
						.keySet()));
			}
		};
		OutputStream output = null;

		try {
			output = new FileOutputStream(configDirectory + FileUtils.SEPARATOR
					+ configFileName);

			prop.putAll(propertyMap);
			// salva properties
			prop.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Carrega arquivo de propriedades da pasta.
	 * 
	 * @param configDirectory
	 *            - Diretorio que arquivo property esta.
	 * @param configFileName
	 *            - Nome do arquivo property.
	 * 
	 * @return Uma entidade de {@link java.util.Properties}.
	 */
	static public Map<String, String> loadPropertyFile(String configDirectory,
			String configFileName) {
		Map<String, String> returnMap = new HashMap<String, String>();
		Properties prop = new Properties();
		InputStream input = null;
		String fullFileName = configDirectory + FileUtils.SEPARATOR
				+ configFileName;
		try {
			input = new FileInputStream(configDirectory + FileUtils.SEPARATOR
					+ configFileName);

			// load a properties file
			prop.load(input);

			for (Entry<Object, Object> entry : prop.entrySet()) {
				returnMap.put((String) entry.getKey(),
						(String) entry.getValue());
			}
		} catch (FileNotFoundException e) {
			LogUtils.fatal("Invalid configuration. Not exists the file - "
					+ fullFileName);
		} catch (IOException ex) {
			LogUtils.fatal("Invalid configuration. It wasn't possible to read the file - "
					+ fullFileName);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return returnMap;
	}

	static public boolean isDirectory(String folderName) {
		return new File(folderName).isDirectory();
	}

	static public void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	static public boolean checkMD5(String configuredFile, String target) {
		try {
			String configuredFileMD5Name = configuredFile + ".MD5";
			LogUtils.debug("MD5 for:" +configuredFileMD5Name);
			if (!new File(configuredFileMD5Name).exists()) {
				LogUtils.debug("No MD5 file for " + configuredFile);
			}
			String md5Target = calculateMD5(target);

			List<String> lines = Files.readAllLines(Paths
					.get(configuredFileMD5Name));

			if (lines.size() != 1) {
				LogUtils.debug("Invalid MD5 for: " + configuredFileMD5Name);
			} else {
				String md5Configurated = lines.get(0);

				if (!md5Configurated.equals(md5Target)) {
					LogUtils.debug("Invalid MD5 check. Inconsistent");

					LogUtils.debug("Configurated: " + md5Configurated);
					LogUtils.debug("Target: " + md5Target);
				} else {
					return true;
				}
			}

			return false;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.debug("Error checking MD5.");
			return false;
		}
	}

	public static String generateMD5File(String fullFileName) {
		try {
			String md5 = calculateMD5(fullFileName);

			String fullFileNameMD5 = fullFileName + ".MD5";
			FileOutputStream md5OutputStream = new FileOutputStream(
					fullFileNameMD5);
			md5OutputStream.write(md5.getBytes());
			md5OutputStream.close();

			return md5;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.debug("Error generating MD5 file: " + e.getMessage());
		}
		return null;
	}

	public static String getNameFilePath(String fileNamePath){
		if(fileNamePath.isEmpty()){
			return "";
		}
		
		try {
			StringUtils.countMatches(fileNamePath,".jar");
			
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+"\nCause: "+e.getCause());
		}
		
		
		return "";
	}
	
	
	public static String calculateMD5(String fullFileName) {
		try {
			byte[] targetBytes = Files.readAllBytes(Paths.get(fullFileName));
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(targetBytes);

			// convert the byte to hex format method 2
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < thedigest.length; i++) {
				String hex = Integer.toHexString(0xff & thedigest[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			LogUtils.debug(hexString.toString());
//System.out.println("MD5: "+hexString.toString() +"Arquivo: "+fullFileName);
			return hexString.toString();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.debug("Error generating MD5 file: " + e.getMessage());
		}
		return null;
	}
	
	public static String readXmlInputFile(String file, String tag_case){
		
		StringBuilder stringFiles = new StringBuilder();
		
		try {
			File fileXml = new File(file);
			DocumentBuilderFactory fileFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = fileFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fileXml);
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName(tag_case);
			System.out.println("\n->Arquivos que servir�o de refer�ncia (xml/properties/cfg): ");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				
				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println(eElement.getTextContent());
					stringFiles.append(eElement.getTextContent()+",");
				}
			}
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage());
		}
		return stringFiles.toString();
		
	}
	
	
	public static boolean getXmlConfigurationTag(String file, String tag_str){
		boolean override = false;
		try{
			File fxml = new File("configuration.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fxml);
			
			doc.getDocumentElement().normalize();
			
			if(doc.getElementsByTagName(tag_str).item(0).getTextContent().matches("true")){
				override = true;
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			LogUtils.debug("Error reading XML Configuration file: " + ex.getMessage());			
		}
		
		return override;
	}
	public static String tratarStringPath(String path){
		String new_path = path.replace("_", "/");
		return new_path;
		
	}
	/**
	 * 
	 * @return
	 */
	public static int countMatchesSeparator(String s){
		int counter = 0;
		String[] values = s.split(FileUtils.SEPARATOR);
		counter = values.length - 1;
		for(String value : values){
			if(value.matches("")){
				counter = counter-1;
			}
		}
		///System.out.println("--"+values.length);
		
		return counter;
	}
	// The method to get all children of the dir
    private static File[] listChildrenFiles(File dir)
    {
        List list = new ArrayList();
        listChildrenFiles(dir, list);
        return (File[]) list.toArray(new File[0]);
    }
     
    // The method to recursively get all children of the dir
    private static void listChildrenFiles(File dir, List list)
    {
        File[] files = null;
        if (dir.isDirectory() && ((files = dir.listFiles()) != null))
        {
            for (int i = 0; i < files.length; i++)
                list.add(files[i]);
            for (int i = 0; i < files.length; i++)
                listChildrenFiles(files[i], list);
        }
    }
  
    public static void deleteDirOnExit(File dir)
    {
        File[] files = listChildrenFiles(dir);
         
        // Call deleteOnExit() in reverse order so hope
        // VM will delete the contents first, then delete 
        // the directory
        for (int i=files.length-1; i>=0; i--)
        {
            files[i].deleteOnExit();
        }
  
    }
    
    public static String replaceEmpty(String value){
    	
    	value = value.replace("\r\n", " ").replace("\n", " ").replace("\\s+", " ").trim();
    	value = value.replace("\n", " ");
    	value = value.replace("\\s+", " ");
    	value = value.replaceAll("\\s+", " ");
    	value = value.trim();
    	return value;
    }
	
    public static String replaceRegex(String line, String c){
    	String line_ = line;
    		try {
    			if(line.length()>0 && !c.isEmpty() ){
						Pattern pattern = Pattern.compile(c);
						Matcher matcher = pattern.matcher(line);
						int a = 0;
						int b = 0;
						while (matcher.find()) {
							a = matcher.start();
							b = matcher.end();
						}
						String head = line.substring(0, a);
						String body = line.substring(b, line.length());
						line_ = head+"_";
						if(b < line.length()){
							line_ = line_+body;	
						}
    				}
			} catch (Exception e) {
				System.out.println("Error converting Regex: "+e.getMessage()+" Cause: "+e.getCause());
			}
    	
    		return line_;
    	
    }
    /**
     * cliente_base/InGRID_4.1.2.1/plugins/com.ibm.icu.nl1_3.4.5.v200609270227.jar
     * = cliente_base/InGRID_4.1.2.1
     * @return
     */
    public static String makeNewDirectory(String a, String b){
    	
    	String result = null;
    	try {
			if(b.endsWith("jar") || b.endsWith("rar") || b.endsWith("war") || b.endsWith("ear") || b.endsWith("zip")){
				String ext = FilenameUtils.getExtension(b);
				String nome = b.replace("."+ext, "");
				nome = FileUtils.extractFileName(nome);
				//result = a+ FileUtils.SEPARATOR+nome;
				result = nome;
			}
		} catch (Exception e) {
			System.out.println("Error: "+e.getCause());
		}
    	
    	
    	return result;
    }
    
    public static boolean isEqualFiles(String path1, String path2){
    	boolean value = false;
    	
    	try {
    		int a = path1.lastIndexOf(".jar");
    		
    		String[] b = path2.split(".jar");
    		
    		if(b.length>1){
    			if((b[b.length-1]+".jar").matches(path1)){
    				value = true;
    			}
    		}
    		//System.out.println(a+"-"+b[b.length-1]);	
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+","+e.getCause());
		}
    	
    	return value;
    }
    /**
     * Remove tha last Jar File.
     * @param filePath
     * @return
     */
    public static String tackOffLastJar(String filePath, String baseName){
    	String ext = FilenameUtils.getExtension(filePath);
    	String split = "";
    	if(!ext.isEmpty()){
    		split = "."+ext;
    	}
    	String pathReturn = "";
    	try {
    		String[] splitPath = filePath.split(split);
    		
    		if(splitPath.length>1){
    			String lastJar = splitPath[splitPath.length-1]+split;
    			int k = filePath.indexOf(lastJar);
    			pathReturn = filePath.substring(0, k);
    		}else{
    			int m = filePath.indexOf(baseName);
    			pathReturn = filePath.substring(0,m+baseName.length());
    					
    		}
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+","+e.getCause());
		}
    	return pathReturn;
    }
    
    
    public static String getPrincipalNameFilePath(String fileNamePath2){
		String frase = "";
		//System.out.println("-->"+fileNamePath2);
		try {			
			int jar = StringUtils.lastIndexOf(fileNamePath2,".jar");
			int war = StringUtils.lastIndexOf(fileNamePath2,".war");
			int ear = StringUtils.lastIndexOf(fileNamePath2,".ear");
			int zip = StringUtils.lastIndexOf(fileNamePath2,".zip");
			int rar = StringUtils.lastIndexOf(fileNamePath2,".rar");
			
			int jarN = StringUtils.countMatches(fileNamePath2,".jar");
			int warN = StringUtils.countMatches(fileNamePath2,".war");
			int earN = StringUtils.countMatches(fileNamePath2,".ear");
			int zipN = StringUtils.countMatches(fileNamePath2,".zip");
			int rarN = StringUtils.countMatches(fileNamePath2,".rar");
			
			int somatoria = jarN + warN + earN + zipN + rarN;
			if(somatoria > 1){
						int biggest = 0;
						String valor = null;
						if ((jar >= war) && (jar >= rar) && (jar >= ear) && (jar >= zip)) { // jar >= b,c,d,e
							biggest = jar;
							valor = ".jar";
						} else if ((war >= rar) && (war >= ear) && (war >= zip)) {      // b >= c,d,e
							biggest = war;
							valor = ".war";
						} else if ((rar >= ear) && (rar >= zip)) {                  // c >= d,e
							biggest = rar;
							valor = ".rar";
						} else if (ear >= zip) {                                // d >= e
							biggest = ear;
							valor = ".ear";
						} else {                                            // e > d
							biggest = zip;
							valor = ".zip";
						}
			
						frase = fileNamePath2.substring(0, biggest);
						
						jar = StringUtils.lastIndexOf(frase,".jar");
						war = StringUtils.lastIndexOf(frase,".war");
						ear = StringUtils.lastIndexOf(frase,".ear");
						zip = StringUtils.lastIndexOf(frase,".zip");
						rar = StringUtils.lastIndexOf(frase,".rar");
			
						int biggest2 = 0;
						String valor2 = null;
						if ((jar >= war) && (jar >= rar) && (jar >= ear) && (jar >= zip)) { // jar >= b,c,d,e
							biggest2 = jar;
							valor2 = ".jar";
						} else if ((war >= rar) && (war >= ear) && (war >= zip)) {      // b >= c,d,e
							biggest2 = war;
							valor2 = ".war";
						} else if ((rar >= ear) && (rar >= zip)) {                  // c >= d,e
							biggest2 = rar;
							valor2 = ".rar";
						} else if (ear >= zip) {                                // d >= e
							biggest2 = ear;
							valor2 = ".ear";
						} else {                                            // e > d
							biggest2 = zip;
							valor2 = ".zip";
						}
						//System.out.println("-)-"+biggest2);
						frase = frase.substring(biggest2 + 5, frase.length());
						//System.out.println("-->"+frase+valor);
						frase = frase+valor;

			}else{
				int biggest = 0;
				String valor = null;
				if ((jar >= war) && (jar >= rar) && (jar >= ear) && (jar >= zip)) { // jar >= b,c,d,e
					biggest = jar;
					valor = ".jar";
				} else if ((war >= rar) && (war >= ear) && (war >= zip)) {      // b >= c,d,e
					biggest = war;
					valor = ".war";
				} else if ((rar >= ear) && (rar >= zip)) {                  // c >= d,e
					biggest = rar;
					valor = ".rar";
				} else if (ear >= zip) {                                // d >= e
					biggest = ear;
					valor = ".ear";
				} else {                                            // e > d
					biggest = zip;
					valor = ".zip";
				}
				frase = fileNamePath2.substring(fileNamePath2.indexOf(FileUtils.SEPARATOR) + 1, fileNamePath2.indexOf(valor) + 4);
			}
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage()+"\nCause: "+e.getCause());
		}
		return frase;
	}
	
    
    
}
