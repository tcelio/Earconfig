package br.com.indra.earconfig.utils;

import br.com.indra.earconfig.process.Function;


/**
 * Classe utilit�ria por armazenar logs das operacoes.
 * Esta centralizado para facilitar amanha adicionar um Log4j ou algo do g�nero.
 * 
 * @author bhenrique
 *
 */
public final class LogUtils {

	private static final boolean DEBUG_ENABLE;
	
	static {
		DEBUG_ENABLE = java.lang.management.ManagementFactory.getRuntimeMXBean().
				getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
	}
	
	public static void fatal(String errorMessage){ 
		fatal(errorMessage, 1);
	}
	
	public static void fatal(String errorMessage, final Function function){
		function.call();
		fatal(errorMessage, 1);
	}
	
	public static void fatal(String errorMessage, int errorCode){
		System.err.println(errorMessage);
		System.exit(errorCode);
	}

	public static void debug(String debugMessage) {
		if (DEBUG_ENABLE){
			System.out.println("[DEBUG] "+debugMessage);
		}
	}
}
