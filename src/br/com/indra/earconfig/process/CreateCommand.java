package br.com.indra.earconfig.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FilenameUtils;

import br.com.indra.earconfig.utils.FileUtils;
import br.com.indra.earconfig.utils.LogUtils;
import br.com.indra.earconfig.utils.ZipUtils;

public final class CreateCommand {
	
	public static final String Properties_file = "input.xml";
	public String FILES_TO_READ = "";                                
	public String CLIENT_JARS_TO_READ = ""; 
	public boolean first_time = true; //este flag � necess�rio para que arquivos JAR consigam ser esxtraidos assim como EAR e extraia os sub projetos JARs, caso existam.
	public static final String Configuration_file = "configuration.xml";
	public boolean SHOW_FILES = false;
	public boolean CLIENT_EVENT = false;
	private final Map<String, String> propertyMap = new HashMap<String, String>();
	private final List<String> acceptFilesList = new ArrayList<String>();
	private final List<String> acceptJARFilesList = new ArrayList<String>();
	private static String zipFileReference_ = "";
	private String oldZipFilePath = "";
	private String zipFilePath = "";
	private String zipReferencia = "";
	private String packageFileNameReference = "";
	private String packageFileNameReferenceTarget = "";
	private Charset UTF8 = Charset.forName("windows-1252");  
	private Charset CP866 = Charset.forName("CP866");  //("CP866");
	
	
	public CreateCommand() {
		if(FileUtils.exists(Properties_file)){
			//aqui a aplicacao obtem os arquivos xml que seriam scaneados.
			this.FILES_TO_READ = FileUtils.readXmlInputFile(Properties_file, "file");
			this.CLIENT_JARS_TO_READ = FileUtils.readXmlInputFile(Properties_file, "jar");
			
		}else{
			System.out.println("\n\narquivo input.xml nao existe ou inacessivel!!!\n\n");
		}
		
		if(FileUtils.exists(Configuration_file)){
			if(FileUtils.getXmlConfigurationTag(Configuration_file, "showUnzipedFiles")){
				this.SHOW_FILES = true;
			}
		}
		acceptFilesList.clear();
		acceptFilesList.addAll(Arrays.asList(FILES_TO_READ.split(",")));
		acceptJARFilesList.clear();
		acceptJARFilesList.addAll(Arrays.asList(CLIENT_JARS_TO_READ.split(",")));
		
		System.out.println("\n\nLista adicionada a memoria. Aguarde ...");
	}
	/**
	 *
	 * @param directory
	 * @param earFile
	 * @param create_type - pode ser cliente (true) ou server (false)
	 */
	public void process(String directory, String earFile, boolean create_type){
				if(create_type){
					this.CLIENT_EVENT = true;
				}
		
				String extension = FilenameUtils.getExtension(earFile);
				String extension_configuration = "";
				if(extension.matches("ear") || extension.matches("jar") || extension.matches("zip") ||
						extension.matches("war") || extension.matches("rar")){
					extension_configuration = "."+extension;
				}				
				if (!FileUtils.exists(directory)){
					FileUtils.createDirs(directory);
				}
				
				try {
					FileUtils.delete(new File(directory));
				} catch (IOException e) {
					e.printStackTrace();
					LogUtils.fatal("Error cleaning config folder. Check if has no opened files from: "+directory);
				}
				propertyMap.clear();
				propertyMap.put("output", FileUtils.extractFileName(earFile).replace(extension_configuration, "_")+ FileUtils.extractFileName(directory)+ extension_configuration);
				System.out.println("Extraindo arquivos de referencia. Aguarde ...");
				
				String baseName = FileUtils.makeNewDirectory(directory, earFile);
				this.zipFilePath = earFile;
				this.zipReferencia = earFile;
				
				System.out.println(Charset.defaultCharset());
				if(CLIENT_EVENT){
					unzip(earFile, directory, earFile, baseName);
				}else{
					unzip(earFile, directory, earFile, baseName);
				}
				
				FileUtils.savePropertyFile(directory,ConfigEAR.EAR_CONFIG_FILENAME, propertyMap);
				System.out.println("Arquivos de referencia criados com sucesso!");
				System.out.println("Aplicacao finalizada com sucesso!");
	}
	
	/**
	 * Extrai um aruivo ZIP especificado em zipFilePath para o diretorio
	 * especificado em destDirectory (cria o diretorio caso nao exista)
	 *
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
	public void unzip(String zipFileReference,/* String zipFilePath, String old_zipFilePath, */ String destDirectory, String fullPathName, String baseName) {
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
			//destDir.deleteOnExit();
		}
		String projectName = "";
		String jarFileName = "";
		
		
		String ext = FilenameUtils.getExtension(this.zipFilePath);//jar ou rar ou ear ou zip ou jar etc
		try {
			
			ZipInputStream zipIn = new ZipInputStream(new FileInputStream(this.zipFilePath),this.UTF8);
			
			if (this.zipFilePath.endsWith(ext)) {
				jarFileName = this.zipFilePath.replace(destDirectory	+ FileUtils.SEPARATOR, "");
				projectName = FileUtils.extractFileName(this.zipFilePath).replace("."+ext, "_");
				this.packageFileNameReference = FileUtils.extractFileName(this.zipFilePath).replace("."+ext, "");
			}
			
			ZipEntry entry = zipIn.getNextEntry();
			this.packageFileNameReferenceTarget = FileUtils.extractFilePath(entry.getName());
			
			while (entry != null) {
				String entryFileName = FileUtils.extractFileName(entry.getName());
				 if (entry.getName().endsWith("jar") || entry.getName().endsWith("rar") ||
						 entry.getName().endsWith("war") || entry.getName().endsWith("ear") ||
						 entry.getName().endsWith("zip")) {
										
							String entryFilePath = FileUtils.extractFilePath(entry.getName());
							// Armazena nome completo do arquivo extraido
							this.zipFilePath = ZipUtils.extractFile(zipIn, destDirectory	+ FileUtils.SEPARATOR + entryFilePath, entryFileName, true);
							String teste ="";
//							if(this.zipFilePath.indexOf("lib/AtivaZeus.jar") >= 0){
//								System.out.println("qq");
//							}
							//se entrar aqui significa que eh um arquivo enzipado do primeiro 
							//nivel ex.: nome/nome.jar/lib/arquivo.txt
							if(this.zipFilePath.indexOf(baseName) == -1){
								
								teste = this.zipFilePath.replace(destDirectory, "");
								teste = teste.substring(1);  
								this.oldZipFilePath = this.oldZipFilePath+ FileUtils.SEPARATOR + teste;
							}else{
								this.oldZipFilePath = this.zipFilePath;
							}
							//System.out.println("--------comeco de >"+this.oldZipFilePath);
							 if(entry.getName().indexOf("BDIDAOClient.jar")>=0){
							//	 System.out.println("ok");
							 }
							unzip(zipFileReference, /*old_zipFilePath,*/ destDirectory, fullPathName, baseName);
					
							//se o novo endereco contem o anterior, vc esta dentro de um pasta do endereco anterior
							//caso contrario, vc saiu do diretorio
				}else{
					String extension = FilenameUtils.getExtension(entryFileName);
					String finalName = "";
					if(SHOW_FILES && !extension.matches("class")){
						System.out.println("Arquivos encontrados: "+entry.getName());
					}
					//se existe na lista de arquivos aceitos
					if (acceptFilesList.indexOf(entryFileName) >= 0) {
						String extractFileName = null;
								//verifica se esta no mesmo nivel do zip/jar principal
								extractFileName = projectName + entry.getName().replace(FileUtils.SEPARATOR, "_");
									finalName = this.oldZipFilePath + FileUtils.SEPARATOR + entry.getName();
									System.out.println("-"+finalName+"--"+entry.getName());
						propertyMap.put(extractFileName, finalName);
						ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
					}
					if(this.zipFilePath.matches(this.zipReferencia)){
						  new File(this.zipFilePath);
							File md5File = new File(this.zipFilePath+".MD5");
					}else{
						 new File(this.zipFilePath).deleteOnExit();
							File md5File = new File(this.zipFilePath+".MD5");
							if (md5File.exists()){
								md5File.deleteOnExit();
							}
					}
					//System.out.println(entryFileName);
                   
				}
					
					zipIn.closeEntry();
					entry = zipIn.getNextEntry();
					if(entry == null){
						//System.out.println("\nm");
						this.oldZipFilePath = FileUtils.tackOffLastJar(this.oldZipFilePath, baseName);
						//System.out.println("------>>acabou o arquivo"+this.oldZipFilePath);
					}
				}
			zipIn.close();
			} catch (Exception e) {
			e.printStackTrace();
			LogUtils.fatal("Error on unzip the file -> "
					+ e.getMessage());
		}
	}

	/**
	 * Extrai um aruivo ZIP especificado em zipFilePath para o diretorio
	 * especificado em destDirectory (cria o diretorio caso nao exista)
	 *
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
	private void unzip_old(String zipFileReference, String zipFilePath, String old_zipFilePath, String destDirectory, String fullPathName, String baseName) {
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		String projectName = "";
		String jarFileName = "";
		
		
		String ext = FilenameUtils.getExtension(zipFilePath);//jar ou rar ou ear ou zip ou jar etc
		try {
			
			ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath),UTF8);
			
			if (zipFilePath.endsWith(ext)) {
				jarFileName = zipFilePath.replace(destDirectory	+ FileUtils.SEPARATOR, "");
				projectName = FileUtils.extractFileName(zipFilePath).replace("."+ext, "_");
				this.first_time = false;
			}
			
			ZipEntry entry = zipIn.getNextEntry();
			
			while (entry != null) {
				//System.out.println("-->"+zipFilePath+"----"+entry.getName());
				String h = entry.getName();
				//aqui verificamos se estamos dentro de um novo jar 
				//em um jar principal de primeiro nivel
				//se estivermos no nivel primario, o fullpathname devera ser limpo
				
				String entryFileName = FileUtils.extractFileName(entry.getName());
				String extension = FilenameUtils.getExtension(entryFileName);
				String finalName = "";
				if(SHOW_FILES && !extension.matches("class")){
					System.out.println("Arquivos encontrados: "+entry.getName());
				}
				//se existe na lista de arquivos aceitos
				if (acceptFilesList.indexOf(entryFileName) >= 0) {
					String extractFileName = null;
					//if(FileUtils.countMatchesSeparator(zipFilePath) > 1){
					
							//verifica se esta no mesmo nivel do zip/jar principal
						if (FileUtils.extractFileName(zipFileReference).matches(FileUtils.extractFileName(jarFileName)) 
								&& acceptFilesList.indexOf(entryFileName) >= 0) {
					
							String j = FileUtils.extractFileNameAndPath(jarFileName);
							j = j.replace(FileUtils.SEPARATOR, "_");
							extractFileName = entry.getName().replace(FileUtils.SEPARATOR, "_");
							finalName = entry.getName();
						}else{
							extractFileName = projectName + entry.getName().replace(FileUtils.SEPARATOR, "_");
//							if(extractFileName.matches("bea_wls9_async_response_WEB-INF_web.xml")){
//								System.out.println("ss");
//							}
							if(zipFilePath.indexOf(baseName) == -1){
								//finalName = old_zipFilePath+ FileUtils.SEPARATOR + finalName;
								String teste = zipFilePath.replace(destDirectory, "");
								teste = teste.substring(1);  
								if(FileUtils.isEqualFiles(teste, old_zipFilePath)){
									
									finalName = old_zipFilePath; 
								}else{
									//old_zipFilePath_ = old_zipFilePath+ FileUtils.SEPARATOR + teste;
									finalName = jarFileName + FileUtils.SEPARATOR + entry.getName();
								}
								
							}else{
								finalName = jarFileName + FileUtils.SEPARATOR + entry.getName();
							}
							
								
						}
						//System.out.println("-"+FileUtils.extractFileName(zipFilePath));
						if(FileUtils.extractFileName(zipFilePath).matches("com.soluzionasf.arqw10.common_1.0.0.jar")){
							System.out.println("qqqqqqqqqq22");
						}
						//System.out.println("-->"+zipFilePath+"----"+entry.getName());
					//	if(zipFilePath.indexOf(baseName) == -1){
					//		finalName = old_zipFilePath+ FileUtils.SEPARATOR + finalName; 
					//	}
					propertyMap.put(extractFileName, finalName);
					//System.out.println("Ainda ha po seguinte arquivo>>>"+zipIn.getNextEntry());
					//fullPathName = "";
					
					ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
				}
				else if (entry.getName().endsWith("jar") || entry.getName().endsWith("rar") ||
						 entry.getName().endsWith("war") || entry.getName().endsWith("ear") ||
						 entry.getName().endsWith("zip")) {

							String old_zipFilePath_ = "";
							String entryFilePath = FileUtils.extractFilePath(entry.getName());
							// Armazena nome completo do arquivo extraido
							String fullFileName = ZipUtils.extractFile(zipIn, destDirectory
									+ FileUtils.SEPARATOR + entryFilePath,
									entryFileName, true);
							
							//Descompacta o arquivo zip extraido.
							if(fullFileName.indexOf("lib/server/BDIDAOClient.jar") >= 0){
								System.out.println("qq");
							}
							if(fullFileName.indexOf("lib/AtivaZeus.jar") >= 0){
								System.out.println("qq");
							}
							if(FileUtils.extractFileName(zipFileReference).matches("com.soluzionasf.arqw10.wl_1.0.0.jar")){
								System.out.println("qqqqqqqqqq");
							}
							
							
							//fullPathName = fullFileName;
//							if(entry.getName().indexOf(baseName) == -1){
//								fullFileName =  entry.getName();//zipFilePath + FileUtils.SEPARATOR + entry.getName();
//							}else{
//								fullFileName = entry.getName();
//								
//							}
							//FileUtils.isEqualFiles(fullFileName, old_zipFilePath);
							if(fullFileName.indexOf(baseName) == -1){
								//finalName = old_zipFilePath+ FileUtils.SEPARATOR + finalName;
								String teste = fullFileName.replace(destDirectory, "");
								teste = teste.substring(1);  
								if(FileUtils.isEqualFiles(teste, old_zipFilePath)){
									old_zipFilePath_ = old_zipFilePath;
								}else{
									old_zipFilePath_ = old_zipFilePath+ FileUtils.SEPARATOR + teste;
								}
								
							}else{
								old_zipFilePath_ = fullFileName;
							}
							old_zipFilePath = old_zipFilePath_;
							//unzip(zipFileReference,fullFileName, old_zipFilePath, destDirectory, fullPathName, baseName);
							new File(fullFileName).deleteOnExit();
							
							File md5File = new File(fullFileName+".MD5");
							if (md5File.exists()){
								md5File.deleteOnExit();
							}
							//se o novo endereco contem o anterior, vc esta dentro de um pasta do endereco anterior
							//caso contrario, vc saiu do diretorio
				}
				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
				System.out.println("next:"+entry+"-"+jarFileName+"--"+fullPathName);
				if(zipIn.getNextEntry() == null){
					System.out.println("acabou o arquivo");
				}
				this.first_time = false;
			}
		
			zipIn.close();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.fatal("Error on unzip the file -> "
					+ e.getMessage());
		}
	}
	
	/**
	 * Extrai um aruivo ZIP especificado em zipFilePath para o diretorio
	 * especificado em destDirectory (cria o diretorio caso nao exista)
	 *
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
/*	private void unzipClient(String zipFileReference, String zipFilePath, String destDirectory) {
		//System.out.println("entrei no metodo unzipClient!");
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		String projectName = "";
		String jarFileName = "";

		try {
			Charset CP866 = Charset.forName("CP866");
			ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath),CP866);
			String extensionFile = FilenameUtils.getExtension(zipFilePath);
			
			if (zipFilePath.endsWith("jar") || zipFilePath.endsWith("rar")|| zipFilePath.endsWith("war") 
					|| zipFilePath.endsWith("zip")	|| zipFilePath.endsWith("ear")) {
				
				jarFileName = zipFilePath.replace(destDirectory	+ FileUtils.SEPARATOR, "");
				projectName = FileUtils.extractFileName(zipFilePath).replace("."+extensionFile, "_");
			
			}
			ZipEntry entry = zipIn.getNextEntry();
			while (entry != null) {
				
				String extractFileName = null;
				String nameFile = null;
				
				String entryFileName = FileUtils.extractFileName(entry.getName());
				String extension = FilenameUtils.getExtension(entryFileName);
				if(SHOW_FILES && !extension.matches("class")){
					System.out.println("Arquivos encontrados no EAR: "+entry.getName());
				}
				//verifica se é arquivo solto do diretorio principal
				if (FileUtils.extractFileName(zipFileReference).matches(FileUtils.extractFileName(jarFileName)) && acceptFilesList.indexOf(entryFileName) >= 0) {
						//this.first_time = false;
						nameFile = FileUtils.takeOffFirstDirectoryPath(entry.getName());
						if(FileUtils.extractFileName(entry.getName()).matches("application.xml")){
							System.out.println("Arquivo analisado: "+entry.getName());
						}
						
						String j = FileUtils.extractFileNameAndPath(jarFileName);
						String w = FileUtils.extractFileName(jarFileName);
						j = j.replace(FileUtils.SEPARATOR, "_");
						//extractFileName = FileUtils.takeOffFirstDirectoryPathPlus(entry.getName()).replace(FileUtils.SEPARATOR, "_");//versao cliente antiga
						extractFileName = entry.getName().replace(FileUtils.SEPARATOR, "_");//versao cliente antiga
						//nameFile = w+FileUtils.SEPARATOR+nameFile;
						//extractFileName = entry.getName().replace(FileUtils.SEPARATOR, "_");//versao nova tentativa de funcionamento
						//	System.out.println("Arquivo geradoA: "+extractFileName);
						propertyMap.put(extractFileName, entry.getName());//versao nova tentaiva de funcioanmento
						ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
						System.out.println(nameFile);
				//entra aqui caso não seja arquivo na pasta principal		
				}else if(!FileUtils.extractFileName(zipFileReference).matches(FileUtils.extractFileName(jarFileName)) && acceptFilesList.indexOf(entryFileName) >= 0){
						//nameFile = FileUtils.takeOffFirstDirectoryPath(jarFileName)+FileUtils.SEPARATOR+entry.getName();
					nameFile = jarFileName+FileUtils.SEPARATOR+entry.getName();
						extractFileName = (jarFileName +FileUtils.SEPARATOR+ entry.getName()).replace(FileUtils.SEPARATOR, "_");
						propertyMap.put(extractFileName, nameFile);//versao nova tentaiva de funcioanmento
						System.out.println(nameFile);
						ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
						//System.out.println("Arquivo geradoB: "+extractFileName);
				}else
							
						//propertyMap.put(extractFileName, jarFileName + FileUtils.SEPARATOR + entry.getName());//versao cliente antiga
						
					//	System.out.println("passei aaqui"+projectName+"...."+jarFileName+">"+first_time);
						//this.first_time = false;
				
				if (entry.getName().endsWith("jar") || entry.getName().endsWith("rar") || entry.getName().endsWith("war") ||
						entry.getName().endsWith("zip") || entry.getName().endsWith("ear")) {

						if(acceptJARFilesList.indexOf(FileUtils.extractFileName(entry.getName())) >= 0){

								String entryFilePath = FileUtils.extractFilePath(entry.getName());
								
								// Armazena nome completo do arquivo extraido
								String fullFileName = ZipUtils.extractFile(zipIn, destDirectory	+ FileUtils.SEPARATOR + entryFilePath, entryFileName, true);
								//Descompacta o arquivo zip extraido.
								unzipClient(zipFileReference, fullFileName, destDirectory);
								new File(fullFileName).deleteOnExit();
								
								File md5File = new File(fullFileName+".MD5");
								if (md5File.exists()){
									md5File.deleteOnExit();
								}
						}

					
				}

				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
				//this.first_time = false;
			}
			zipIn.close();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.fatal("Error on unzipClient the EAR file -> "
					+ e.getMessage());
		}
	}*/
}
