package br.com.indra.earconfig.process;

import java.util.Map;

import org.docopt.EnumOptions;

import br.com.indra.earconfig.utils.FileUtils;
import br.com.indra.earconfig.utils.LogUtils;

/**
 * Classe principal respons�vel por validar os comandos aceitos.
 * Ela ira determinar qual comando est� executado e encaminhar o processamento a classe respons�vel.
 * 
 * @author bhenrique
 *
 */
public final class ConfigEAR {

	public static final String EAR_CONFIG_FILENAME = "earconfig.properties";

	/**
	 * Metodo para processar o comando e definir o que ser� realizado.
	 * 
	 * @param opts MAP DE OPTIONS RECEBIDOS. 
	 */
	public void process(Map<String, Object> opts) {
		
		String earFile = (String) opts.get(EnumOptions.EAR_FILE.toString());
		//earFile = earFile.toLowerCase();
		
		boolean create = (boolean) opts.get(EnumOptions.CREATE.toString());
		//boolean update = (boolean) opts.get(EnumOptions.UPDATE.toString());
		//boolean test = (boolean) opts.get(EnumOptions.TEST.toString());
		boolean apply = (boolean) opts.get(EnumOptions.APPLY.toString());
		
		String file = (String) opts.get(EnumOptions.FILE.toString());
		String directory = (String) opts.get(EnumOptions.DIRECTORY.toString());
		String output    = (String) opts.get(EnumOptions.OUTPUT.toString());
		String client    = (String) opts.get(EnumOptions.CLIENT.toString());
	   // System.out.println("->"+directory+"---"+file+"---"+output);
		
		
		String fileOrDirectory = "";
		
		if (file != null){
			fileOrDirectory = file;
		} else {
			fileOrDirectory = directory;
		}
		
//		if (!earFile.endsWith(".ear")) {
//			LogUtils.fatal("Invalid EAR File. For all commands, the EAR file need to be valid.");
//		}
		
		if (!FileUtils.exists(earFile)) {
			LogUtils.fatal("Invalid EAR File. For all commands, the EAR file need to exists.");
		}
		
		if (create){
			if(client != null && client.matches("true")){
				new CreateCommand().process(fileOrDirectory, earFile,true);
			}else{
				new CreateCommand().process(fileOrDirectory, earFile, false);
			}
			
			//System.out.println(">>> "+fileOrDirectory);
		} else if (apply){
			if (file != null){
				new ApplyCommand().processFile(fileOrDirectory, earFile);
				
			} else {
				//System.out.println(">>"+fileOrDirectory);
				new ApplyCommand().processFolder(fileOrDirectory, earFile, output);
				
			}
		}
	}
}
