package br.com.indra.earconfig.process;

@FunctionalInterface
public interface Function {
    void call();
}