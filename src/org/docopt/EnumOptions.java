package org.docopt;

public enum EnumOptions {
	CREATE("create"),
	UPDATE("update"),
	TEST("test"),
	APPLY("apply"),
	FILE("--file"),
	DIRECTORY("--dir"),
	EAR_FILE("<earfile>"),
	OUTPUT("--output"),
	CLIENT("--client");
	
	private String token;

	private EnumOptions(String token){
		this.token = token;
	}
	
	@Override
	public String toString() {
		return token;
	}
}
